# -*- coding: utf-8 -*-
SECRET_KEY = "g1b6x#o&in=$(_8^o7$8kyl29p-%&d6jfj284(j&x6b&ub8f%r"

#
# General Settings
#
TIME_ZONE = 'America/Chicago'

#
# E-Mail Settings
#
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp-relay.sendinblue.com'
EMAIL_HOST_USER = 'nmspencer@icloud.com'
EMAIL_HOST_PASSWORD = 'xsmtpsib-24c5607fdc35ff35a6cb233d07d7aba28357cc4b5da094da6ec1c31259ac6dd7-XE2rkYtvTj3yWSFp'
EMAIL_PORT = 587
DEFAULT_FROM_EMAIL = 'thoth@zyradyl.moe'
SERVER_EMAIL = 'thoth@zyradyl.moe'

#
# MariaDB Settings
#
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'seahub-db',
        'USER': 'seafile',
        'PASSWORD': '6G62eYoA4Di4ysPQZw9233X6gAMBZa',
        'HOST': '127.0.0.1',
        'PORT': '3306'
    }
}

#
# MemcacheD Settings
#
CACHES = {
    'default': {
        'BACKEND': 'django_pylibmc.memcached.PyLibMCCache',
        'LOCATION': '127.0.0.1:11211',
    },
    'locmem': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
}
COMPRESS_CACHE_BACKEND = 'locmem'
