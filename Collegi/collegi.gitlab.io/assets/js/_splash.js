/* jshint strict: false */

var $splash       = $('.page__hero--overlay');

var backgrounds = [
    { src: '/assets/images/splash/01.png', valign: 'center' },
    { src: '/assets/images/splash/02.png', valign: 'center' },
    { src: '/assets/images/splash/03.png', valign: 'center' },
    { src: '/assets/images/splash/04.png', valign: 'center' },
    { src: '/assets/images/splash/05.png', valign: 'center' },
    { src: '/assets/images/splash/06.png', valign: 'center' },
    { src: '/assets/images/splash/07.png', valign: 'center' },
    { src: '/assets/images/splash/08.png', valign: 'center' },
    { src: '/assets/images/splash/09.png', valign: 'center' },
    { src: '/assets/images/splash/10.png', valign: 'center' }
];

$('html').addClass('animated');

var displayBackdrops = false;

$splash.vegas({
    preload: true,
    overlay: '/assets/images/vendor/vegas/overlays/01.png',
    transitionDuration: 4000,
    delay: 10000,
    slides: backgrounds,
    walk: function (nb, settings) {
        if (settings.video) {
            $('.logo').addClass('collapsed');
        } else {
            $('.logo').removeClass('collapsed');
        }
    }
});
