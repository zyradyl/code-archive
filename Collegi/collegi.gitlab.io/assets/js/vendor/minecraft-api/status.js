MinecraftAPI.getServerStatus('collegi.mcserver.ws', function (err, status) {

  // Convert strings to integers to make life easier.
  cur_players = +status.players.now;
  max_players = +status.players.max;

  // Notices have the text colour mixed with white at 90%
  danger_notice = '#fdefef';
  warning_notice = '#fef4e6';
  success_notice = '#eff9ef';

  // Should be comfortable with laying these over equivilent notice colours.
  success_text = '#62c462';
  warning_text = '#f89406';
  danger_text = '#ee5f5b';

  // Error was encountered trying to communicate with the server
  if (err) {
    $('.server').css('background-color', warning_notice);
    $('.server-status').css('color', warning_text);
    $('.server-status').html('Error Reaching Server');
    $('.players-current').html('???');
    $('.players-max').html('???');
    $('.players-level').html('???');
  }

  // Server is Online
  if (status.online === true) {
    $('.server').css('background-color', success_notice);
    $('.server-status').css('color', success_text);
    $('.server-status').html('Online');
    $('.players-current').html(status.players.now);
    $('.players-max').html(status.players.max);

    // Check the percentage of max slots currently filled and then print an
    // appropriate traffic notice.
    if (status.players.now == 0) {
      // No players online, Low traffic condition.
      $('.players-level').css('color', success_text);
      $('.players-level').html('Empty');
    } else if (((cur_players/max_players)*100) <= 33) {
      // Less than 33% of slots filled, Low Traffic Condition.
      $('.players-level').css('color', success_text);
      $('.players-level').html('Low');
    } else if (((cur_players/max_players)*100) <= 66) {
      // Less than 66% of slots filled, Medium Traffic Condition.
      $('.players-level').css('color', warning_text);
      $('.players-level').html('Medium');
    } else if (((cur_players/max_players)*100) <= 100) {
      // Greater than 66% of slots filled, High traffic condition.
      $('.players-level').css('color', danger_text);
      $('.players-level').html('High');
    } else if (cur_players == max_players) {
      // All Slots filled. High traffic condition.
      $('.players-level').css('color', danger_text);
      $('.players-level').html('Full');
    }
  }

  // Server is offline
  if (status.online === false) {
    $('.server').css('background-color', danger_notice);
    $('.server-status').css('color', danger_text);
    $('.server-status').html('Offline');
  }
});
