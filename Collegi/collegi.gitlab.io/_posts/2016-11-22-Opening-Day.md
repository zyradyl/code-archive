---
layout: single
title: "Opening Day on Collegi"
date: 2016-11-22T19:00:00-05:00
author: Zyradyl
---
**Collegi will be starting a slow open on Wednesday, November 23, 2016.**
What this means, is that we will be asking you, our current user base, to begin
to invite your friends to come and join us on Collegi. This will allow our
player base to begin growing organically.

First though, a few disclaimers:

* We're not actually out of beta. I seriously wish I could say that we were,
but this is out of our hands.
* You will need to download new software more often while playing on our server
than you would have to on other servers. This is out of our hands as well. The
various mods that we use to make Collegi what it is have decided to continue to
exist in an almost perpetual beta.
* Things will be broken, and you will need to file bug reports. We will keep
issuing the bounty on bug reports, so that should at least keep you eager to
contribute. Bugs will be plentiful, and bug hunting could be a lucrative source
of Cols for now.

If you know people that would be OK with that, and you are comfortable with
inviting them, please do. We look forward to growing a player base here on
Collegi with everyone's help.
