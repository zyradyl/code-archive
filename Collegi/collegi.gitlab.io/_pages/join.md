---
layout:    splash
title:     "Join Collegi Pixelmon"
date:      2016-11-22T19:00:00-05:00
permalink: /join/
---
# Mod Tutorial #

<iframe width="560" height="315" src="https://www.youtube.com/embed/MzFL-b40qA0" frameborder="0" allowfullscreen></iframe>

**Note:** This video needs updating, but the installation method is still
accurate. View the video on youtube if you would like easy access to jumping
ahead to your operating system.

Below are the current versions of the Mods that are required to play on Collegi:
* Minecraft 1.10.2\*
* [Forge 1.10.2 - 12.18.3.2202](http://files.minecraftforge.net/)\*
* [JourneyMap 5.4.4](http://journeymap.info/Download)\*
* [Pixelmon 5.0.1](http://pixelmonmod.com/downloads.php)
* [Biomes O Plenty 1.10-5.0.0-2109](https://mods.curse.com/mc-mods/minecraft/220318-biomes-o-plenty#t1:other-downloads)

Additionally, There is a required resource pack to play on Collegi [See Notes]:
* [Collegi Resource Pack v0.1](https://gitlab.com/collegi/collegi-resource-pack/uploads/03dcc62efcd555af4813897d3655bd33/Collegi-0.1.0.zip)

Recommended Resource Packs:
* [Sphax Resource Pack For 1.10.2](http://bdcraft.net/purebdcraft-minecraft)
* [Sphax Biomes O' Plenty Patch for 1.10.2](http://bdcraft.net/community/pbdc-patches-wip/biomes-plenty-t1098.html)

### Resource Pack Installation Notes ###
The resource packs need to be activated in a specific order from within
Minecraft. Please first enable the Sphax base pack, then enable the Biomes O'
Plenty patch for Sphax, and finally enable our resource pack. If upon starting
the game, some items seem to be missing from your inventory, enable and disable
the resource packs again. This is a known issue with Pixelmon, and we apologize.

### Forge Notes ###
Please note that if the version of Forge you are running is newer than the
version we recommend you'll still be able to play. The only problem will come
from using an older version. That being said, if you encounter issues with the
other mods, it might be worth trying our recommended forge version.

### JourneyMap Notes ###
While JourneyMap is not required, we strongly recommend it. Additionally, if it
is discovered that you are using an alternative MiniMap to gain an unfair
advantage, it could be considered an exploit. Using JourneyMap means you'll be
using the same MiniMap as server staff, and could even allow you to share
waypoints with other players.

### Minecraft Notes ###
This should go without saying, but we will never support offline or cracked
clients. Additionally, using any modified clients is grounds for an immediate,
permanent ban. Don't take the risk. Use the official client.

### Pixelmon Launcher Notes ###
We do eventually plan to make a mod pack available through the Official Pixelmon
launcher, and when that happens we will provide a link to it. In the mean time,
it should be possible to use the Pixelmon Launcher to get onto Collegi, but it
is not supported at this time.

### Alternative Launcher Notes ###
It may be fully possible to access Collegi through the use of alternative
launchers, but as with the Pixelmon Launcher note, we cannot support these
methods. There are simply too many third party launchers available. In the
future, however, we do plan to support and provide a video for the use of
MultiMC.

### Resource Pack Notes ###
Not using the Collegi Resource Pack can result in some of our custom NPCs
showing up as purple and black missing texture blocks. This will not impact the
function of those NPCs. If you are okay with this, you do not need the resource
pack. The other resource packs (Sphax) are just recommended as Collegi was
designed using them.
