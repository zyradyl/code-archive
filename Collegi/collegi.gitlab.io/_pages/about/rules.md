---
layout:     single
title:      "Collegi Rules"
date:       2016-11-22T19:00:00-05:00
permalink:  /about/rules/
sidebar:
  nav:  "about"
---
 1. Be respectful of other players and staff.
 2. Staff have the final decisions in all matters/controversy.
 3. No controversial topics in chat. Collegi is a place to escape the world,
 not discuss it.
 4. Controversial topics include: Politics, Religion, Sexuality, Gender, and
 both Mental/Physical Health Issues
 5. Everyone is welcome on Collegi. If staff asks you to drop a topic, do so
 immediately.
 6. Hacks, Exploits, X-Ray mods are not allowed. If unsure of a mod, just ask
 staff.
 7. Using a banned mod without asking/maliciously is immediately and
 permanently bannable.
 8. Swearing is allowed, but cursing out other players is absolutely not.
 9. Disrespecting or harrassing other players will lead to a mute and may lead
 to a ban.
 10. Using any type of slur, regardless of context, is a zero tolerance offense.
 11. We allow griefing and looting in the wilderness, NOT in towns.
 12. If you don't have a town and you are griefed, you have only yourself to
 blame.
 13. The pixelmon battle UI is invasive. As such, please ASK a player before
 battling.
 14. Not asking and repeatedly toggling the battle screen, or spam asking,
 is warnable. We understand accidents happen, but don't make it a habit.
 15. Have fun!
