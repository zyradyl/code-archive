require './lib/events/messages'

# Extend the Discord Event container to allow for arbitrary commands to be ran
# when Seara detects a message in a discord chat.
module MessageHandler
  include Messages
  extend Discordrb::EventContainer

  # Called when a message is sent in a channel
  message do |event|
    Messages.process(event)
  end

  # Called when a message is edited.
  message_edit do |event|
    Messages.process(event)
  end
end
