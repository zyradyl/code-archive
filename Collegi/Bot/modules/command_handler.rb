require './lib/commands/bot'
require './lib/commands/minecraft'
require './lib/commands/staff'

# This extends DiscordRB's Command Container to allow Seara to process custom
# commands.
module CommandHandler
  include MinecraftCommands
  include BotCommands
  include StaffCommands
  extend Discordrb::Commands::CommandContainer

  # Commands related to the functionality of Seara herself.
  command :bot, help_available: false do |event|
    BotCommands.process(event)
  end

  # Commands related to the minecraft server.
  command :minecraft, help_available: false do |event|
    MinecraftCommands.process(event)
  end

  # Commands available for staff to run.
  command :staff, help_available: false do |event|
    StaffCommands.process(event)
  end
end
