module ChatUtils

  def self.logging(event, command, extras = '')
    message = Time.now.getutc.strftime("%Y-%m-%d %H:%M:%S %z:")
    case command
    when 'startup'
      message = message + ' ' + CONFIG['messages']['logging'][command]
    when 'channel_create'
      message = message + ' ' + CONFIG['messages']['logging'][command]
      message = message + ' ' + "#{event.channel.id}"
      message = message + ' ' + event.channel.name
      message = message + ' ' + "#{event.channel.type}"
    when 'channel_delete'
      message = message + ' ' + CONFIG['messages']['logging'][command]
      message = message + ' ' + "#{event.id}"
      message = message + ' ' + event.name
      message = message + ' ' + "#{event.type}"
    when 'mc_login'
      message = message + ' ' + CONFIG['messages']['logging'][command]
      message = message + ' ' + extras
    when 'mc_logoff'
      message = message + ' ' + CONFIG['messages']['logging'][command]
      message = message + ' ' + extras
    when 'mc_started'
      message = message + ' ' + CONFIG['messages']['logging'][command]
    when 'mc_stopped'
      message = message + ' ' + CONFIG['messages']['logging'][command]
    else
      message = message + ' ' + "#{event.user.id}"
      message = message + ' ' + '-' + ' ' + event.user.name
      message = message + ' ' + '-' + ' ' + CONFIG['messages']['logging'][command]
      message = message + ' ' + extras
    end
    SEARA.send_message(CONFIG['channels']['bot_monitoring'], message)
  end

  def self.responder(event, message)
    event.channel.start_typing
    if event.channel.id.to_s.eql?(CONFIG['channels']['spongebridge_chat']) && event.message.from_bot?
      name = event.message.content.gsub(/\`/, '').split('>').first
      event.message.reply(name + ', ' + message)
    else
      event.message.reply(event.message.user.mention + ', ' + message)
    end
  end

  def self.temporary(event, message)
    if event.channel.id.eql?(CONFIG['channels']['spongebridge_chat'].to_i)
      event.user.pm(message)
    else
      event.channel.start_typing
      event.channel.send_temporary_message(message, 30)
    end
  end
end
