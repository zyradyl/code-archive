# These are commands that are responsible for controlling how Seara runs.
module BotCommands
  include ChatUtils

  # Spit out an error message.
  def self.cmd_error(event)
    SEARA.send_message(                           \
      event.channel.id,                           \
      CONFIG['messages']['bot_commands']['error'] \
    )
  end

  # Spit out a help message.
  def self.cmd_help(event)
    event.message.delete
    CONFIG['messages']['bot_commands']['help'].each do |_key, value|
      ChatUtils.temporary(event, value)
    end
  end

  # Let Seara get some rest.
  def self.cmd_sleep(event)
    ChatUtils.logging(event, 'shutdown')
    SEARA.send_message(event.channel.id, 'Goodnight.')
    exit
  end

  # Process the command message.
  def self.process(event)
    return unless CONFIG['bot']['privileged-users'].include?(event.user.id)
    command = event.content.strip.split(' ')
    case command[1]
    when 'sleep'
      cmd_sleep(event)
    when 'help'
      cmd_help(event)
    else
      cmd_error(event)
    end
  end
end
