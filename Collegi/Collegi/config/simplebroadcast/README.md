# Collegi Pixelmon Server #
## Broadcast Directory ##

### Description ###
This file contains the messages that are posted to the server by the automatic
broadcaster. Colour codes and other such things can be added to the file to
enable different features, refer to the *templates.txt* file for more
information.
